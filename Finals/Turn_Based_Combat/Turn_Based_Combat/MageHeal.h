#pragma once
#include "Skill.h"
#include <string>
#include <iostream>
using namespace std;

class Unit;

class MageHeal : public Skill
{
public:
	MageHeal(string name);
	~MageHeal();

	void use(Unit* chance, vector<Unit*> &targets);

};

