#pragma once
#include "Skill.h"
#include <string>
#include <iostream>

class Unit;

using namespace std;

class WarriorShockwave : public Skill
{
public:
	WarriorShockwave(string name);

	void use(Unit* chance, vector<Unit*> &targets);

	~WarriorShockwave();

private:
	float mDamageCoefficient;
};

