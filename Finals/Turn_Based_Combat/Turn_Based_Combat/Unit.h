#pragma once
#include <string>
#include <iostream>
#include <vector>
using namespace std;
#include "Skill.h"
#include "MageHeal.h"
#include "WarriorShockwave.h"
#include "AssassinAssassinate.h"


class Unit
{
public:
	Unit(string name, string teamSide, int classRole);

	string getName();
	string getTeamSide();
	string getSkillName();

	int damageComputation(Unit* player, Unit* target);

	int getMaxHp();
	int getHp();
	int getMaxMp();
	int getMp();


	int getPower();
	int getVitality();
	int getAgility();
	int getDexterity();

	//determining the role of the class
	int getClassRole();
	int getManaCost();

	//fate of the health of the Units
	bool fate();

	bool hitAttack(Unit* chance, Unit* target);

	void printStats();
	void takeDamage(int damage);
	void heal();
	void skill(Unit* player, vector<Unit*> &target);
	void decreaseMp(int value);


	~Unit();

private:

	Skill *mSkill;

	string mName;
	string mTeamSide;

	int mPow;
	int mVit;
	int mAgi; 
	int mDex;

	int mMaxHp;
	int mMaxMp;
	int mMp;
	int mHp;

	//determining the role of the class
	int mClassRole;

};

