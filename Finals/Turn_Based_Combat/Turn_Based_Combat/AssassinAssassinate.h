#pragma once
#include "Skill.h"
#include <string>
#include <iostream>
using namespace std;

class Unit;

class AssassinAssassinate : public Skill
{
public:
	AssassinAssassinate(string name);

	void use(Unit* chance, vector<Unit*> &targets);

	~AssassinAssassinate();

private:
	float mDamageCoefficient;
};

