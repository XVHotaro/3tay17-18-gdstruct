#include "WarriorShockwave.h"
#include "Unit.h"

WarriorShockwave::WarriorShockwave(string name) :Skill(name)
{
	mCost = 5;
	mDamageCoefficient = 0.9f;
}

WarriorShockwave::~WarriorShockwave()
{
}

void WarriorShockwave::use(Unit * chance, vector<Unit*>& targets)
{
	//SKill Shockwave
	cout << chance->getName() << " used " << chance->getSkillName() << endl;

	for (int j = 0; j < targets.size(); j++){

		if (chance->getTeamSide() != targets[j]->getTeamSide()){

			Unit* target = targets[j];

			int damage = chance->damageComputation(chance, target) * mDamageCoefficient;
			target->takeDamage(damage);

			cout << targets[j]->getName() << " took " << damage << " damage! ";
		}
	}

	//Aftermath
	int cost = getCost();
	chance->decreaseMp(cost);
}