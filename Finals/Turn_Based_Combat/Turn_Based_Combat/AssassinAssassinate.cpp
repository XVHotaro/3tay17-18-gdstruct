#include "AssassinAssassinate.h"
#include "Unit.h"

AssassinAssassinate::AssassinAssassinate(string name):Skill (name)
{
	//cost of Skill
	mCost = 5;
	mDamageCoefficient = 2.2f;
}

void AssassinAssassinate::use(Unit * chance, vector<Unit*>& targets)
{
	//This'll target enemies(duh)
		vector<Unit*> enemies;
		
		for (int j = 0;j < targets.size();j++){
			//this'll make the player team identify who to attack
			if (chance->getTeamSide() != targets[j]->getTeamSide()) { enemies.push_back(targets[j]); }
		}

		Unit* target = enemies[0];

		for (int f = 0; f < enemies.size(); f++){
			if (target->getHp() >= enemies[f]->getHp() && chance->getTeamSide() != enemies[f]->getTeamSide()) { target = enemies[f]; }
		}

		//damage calculation
		int damage = chance->damageComputation(chance, target) * mDamageCoefficient;
		target->takeDamage(damage);

		cout << chance->getName() << " used " << chance->getSkillName() << " on " << target->getName() << endl;
		cout << target->getName() << " took " << damage << " damage! " << endl;

		//Mana Reducer
		int cost = getCost();
		chance->decreaseMp(cost);
}


AssassinAssassinate::~AssassinAssassinate()
{
}
