#include "MageHeal.h"
#include "Unit.h"


MageHeal::MageHeal(string name) :Skill(name)
{
	//cost of the Mage Heal Skill
	mCost = 2;
}

MageHeal::~MageHeal()
{
}

void MageHeal::use(Unit * chance, vector<Unit*>& targets)
{
	//this'll set the range for the Mage to heal
	vector<Unit*> range;
	for (int j = 0; j < targets.size(); j++){

		if (chance->getTeamSide() == targets[j]->getTeamSide()) 
		{ range.push_back(targets[j]); }
	}

	//targeting the range for the Mage to heal
	Unit* target = range[0];

	//Applying the Healing Skill
	for (int f = 0; f < range.size(); f++) { 
		
		if (range[f]->getHp() <= target->getHp()) { target = range[f]; }
	}


	cout << chance->getName() << " casted Heal " << chance->getSkillName() << " on " << target->getName() << endl;


	//Implementation of Healing whether it's needed
	if (target->getHp() <= target->getMaxHp()){
		target->heal();
		cout << chance->getName() << " healed " << target->getName() << endl;
	}
	//or not
	else { cout << "No one doesn't need to heal. " << endl; }

	int cost = getCost();
	chance->decreaseMp(cost);
}