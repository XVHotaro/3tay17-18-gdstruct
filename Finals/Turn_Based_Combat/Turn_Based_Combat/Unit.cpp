#include <iostream>
#include <string>

#include "Unit.h"
#include "Skill.h"
#include "MageHeal.h"
#include "WarriorShockwave.h"
#include "AssassinAssassinate.h"

Unit::Unit(string name, string teamSide, int classRole)
{
	mName = name;
	mTeamSide = teamSide;
	mClassRole = classRole;

	mMaxHp = rand() % 80 + 20;
	mHp = mMaxHp;
	
	mMaxMp = rand() % 25 + 5;
	mMp = mMaxMp;

	mPow = rand() % 19 + 30;
	mVit = rand() % 9 + 20;
	mDex = rand() % 9 + 30;
	mAgi = rand() % 9 + 30;

	
	if (classRole == 1) { mSkill = new WarriorShockwave("Shockwave"); }

	else if (classRole == 2) { mSkill = new MageHeal("Heal"); }

	else if (classRole == 3) { mSkill = new AssassinAssassinate("Assassinate"); }
}

string Unit::getName()
{
	return mName;
}

string Unit::getTeamSide()
{
	return mTeamSide;
}

string Unit::getSkillName()
{
	return mSkill->getName();
}

int Unit::damageComputation(Unit * player, Unit * target)
{
	int variable = mPow * .20;
	int damage = rand() % variable + mPow;

	// xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx   Class Role Strenghts & Weaknesses   xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
	//===============================================================================================================================
	if (player->getClassRole() == 1 && target->getClassRole() == 2) { damage = (damage - target->getVitality()) * 1.5; }//===========
	//===============================================================================================================================
	else if (player->getClassRole() == 2 && target->getClassRole() == 3) { damage = (damage - target->getVitality()) * 1.5; }//======
	//===============================================================================================================================
	else if (player->getClassRole() == 3 && target->getClassRole() == 1) { damage = (damage - target->getVitality()) * 1.5; }//======
	//===============================================================================================================================

	else { damage = (damage - target->getVitality()); }

	if (damage < 1) { damage = 1; }
	return damage;

}

int Unit::getMaxHp()
{
	return mMaxHp;
}

int Unit::getHp()
{
	return mHp;
}

int Unit::getMaxMp()
{
	return mMaxMp;
}

int Unit::getMp()
{
	return mMp;
}

int Unit::getPower()
{
	return mPow;
}

int Unit::getVitality()
{
	return mVit;
}

int Unit::getAgility()
{
	return mAgi;
}

int Unit::getDexterity()
{
	return mDex;
}

int Unit::getClassRole()
{
	return mClassRole;
}

int Unit::getManaCost()
{
	int cost = mSkill->getCost();
	return cost;
}

bool Unit::fate()
{
	if (mHp > 0){return true;}

	else{return false;}
}

bool Unit::hitAttack(Unit* chance, Unit* target)
{
	int hit = (chance->getDexterity() / target->getAgility()) * 100;

	if (hit < 10) { hit = 10; }

	if (hit > 60) { hit = 60; }

	int random = rand() % 100 + 1;

	if (random <= hit) { return true; }

	else { return false; }
}

void Unit::printStats()
{
	//CLASS ROLES
	//Warrior
	if (mClassRole == 1) { cout << "CLASS: Warrior " << endl; }
	//Mage
	else if (mClassRole == 2) { cout << "CLASS: Mage " << endl; }
	//Assassin
	else if (mClassRole == 3) { cout << "CLASS: Assassin " << endl; }
	//Le Statistico
	cout << "HP = " << mHp << "/" << mMaxHp << endl;
	cout << "POW = " << mPow << endl;
	cout << "VIT = " << mVit << endl;
	cout << "AGI = " << mAgi << endl;
	cout << "DEX = " << mDex << endl;
	cout << "MP = " << mMp << endl;
}

void Unit::takeDamage(int damage)
{
	mHp -= damage;
	if (mHp < 0){mHp = 0;}
}

void Unit::heal()
{
	//Healing the 0.3 of your health
	mHp += mMaxHp * .30;
	//so that it won; got out of range for the health
	if (mHp > mMaxHp) { mHp = mMaxHp; }
}

void Unit::skill(Unit * player, vector<Unit*>& targets)
{
	mSkill->use(player, targets);
}

void Unit::decreaseMp(int value)
{
	mMp = mMp - value;
}

Unit::~Unit()
{
}
