#pragma once
#include <string>
#include <iostream>
#include <vector>
using namespace std;

class Unit;

class Skill
{
public:
	Skill(string name);
	~Skill();

	string getName();
	int getCost();

	// doing these will make you access the base class shit or something of the virtual
	//not only that, using the vector will make it target 3 enemies/players continously
	virtual void use(Unit* player, vector<Unit*> &targets);


protected: 
	string mName;
	int mCost;
};

