#include <iostream>
#include <string>
#include <vector>
#include <time.h>
#include <algorithm>
using namespace std;
#include "Unit.h"
#include "Skill.h"
#include "AssassinAssassinate.h"
#include "MageHeal.h"
#include "WarriorShockwave.h"

//Printing of the Turn order for the Player Side and Enemy Side
vector<Unit*> Order(vector<Unit*> &sideJ, vector<Unit*> &sideE)
{
	vector<Unit*> turnOrder;
	//Player Turn Order
	for (int j = 0; j < 3; j++) { turnOrder.push_back(sideJ[j]); }
	//Enemy Turn Order
	for (int f = 0; f < 3; f++) { turnOrder.push_back(sideE[f]); }

	//this is where the declaration "algorithm" is used 
	// for some reason it needs "[]" this, i don't know how or why
	sort(turnOrder.begin(), turnOrder.end(), [](Unit* sideJ, Unit* sideE) {return sideJ->getAgility() > sideE->getAgility(); });
	//for setting the Agility so that when the before they kill each other, the evasion rate will be set on the function
	return turnOrder;
}

void shop(int currency, int choice)
{
	cout << "What do you want to buy?" << endl;
	cout << "1 - Potion = 50g" << endl;
	cout << "2 - Herbs = 30g" << endl;
	cout << "3 - Gems = 30000php" << endl;

	cin >> choice;
	switch (choice)
	{
	case '1':
		cout << "You bought potion! Which is a total scam." << endl;
		cout << "Minus 50g" << endl;
		currency -= 50;
		break;
	case '2':
		cout << "You bought Herbs! Which is another scam." << endl;
		cout << "Minus 30g" << endl;
		currency -= 30;
		break;
	case '3':
		cout << "You do not have enough php, you cannot purchase this item!!" << endl;
		break;
	default:
		break;
	}
}

void menuCheck(int input, vector<Unit*> &player, vector<Unit*> enemy, int currency)
{
	int choice = 0;

	cout << "You are currently in Novice Town." << endl;
	cout << "It is a town for new beginnings and nurturing adventurers." << endl;
	cout << "What would you like to do?" << endl;

	cout << "1 - Visit the shop" << endl;
	cout << "2 - Battle your rival" << endl;
	cout << "3 - Talk to Village Elder" << endl;
	cout << "4 - Talk to Mom" << endl;
	cout << "5 - Check Status" << endl;
	cout << "6 - Leave Novice Town" << endl;

	cin >> input;

	switch (input)
	{
	case '1'://shop
		shop(currency, choice);
		break;
	case '2'://battle rival
		playTurn(player, enemy);
		break;
	case '3'://talk to village elder
		cout << "Village Elder: What do you need?" << endl;
		cout << "Nothing. Go back." << endl;
		break;
	case '4': //talk to mom
		cout << "Mom: Do you need anything?" << endl;
		cout << "Mom: Oops, gotta go now, bye." << endl;
		break;
	case '5' ://check status
		player[3]->printStats();
		break;
	case '6': // leave town
		cout << "Do you want to leave Novice town?" << endl;
		cout << "Ok bye." << endl;
		break;

	default:
		cout << "You are currently in Novice Town." << endl;
		cout << "It is a town for new beginnings and nurturing adventurers." << endl;
		cout << "What would you like to do?" << endl;

		cout << "1 - Visit the shop" << endl;
		cout << "2 - Battle your rival" << endl;
		cout << "3 - Talk to Village Elder" << endl;
		cout << "4 - Talk to Mom" << endl;
		cout << "5 - Check Status" << endl;
		cout << "6 - Leave Novice Town" << endl;

		cin >> input;
	}
}

//Shows the Turn Order of which members of the team goes first
void turnOrderPrint(vector<Unit*> &turn)
{
	cout << "=========== TURN ORDER ===========" << endl;

	for (int j = 0; j < turn.size(); j++) { cout << j + 1 << turn[j]->getTeamSide() << turn[j]->getName() << endl; }

	cout << "=========== TURN ORDER ===========" << endl;
}

//The Attack Function
Unit* locateTarget(Unit* chance, vector<Unit*> &targets)
{
	vector<Unit*> enemies;
	for (int j = 0; j < targets.size(); j++){
		//this would distinguish the difference between the player and enemies
		if (chance->getTeamSide() != targets[j]->getTeamSide()) { enemies.push_back(targets[j]); }
	}

	//this would randomize on who will the player attack from inside the stored variables in the vectors
	Unit* target = enemies[rand() % enemies.size()];
	return target;
}

//This function will determine whether it's critical or not for the basic Attack
bool criticalHit(Unit* chance, Unit* target)
{
	//Critical Attack Hit
	int critical = 20;
	int random = rand() % 100 + 1;

	if (random <= critical) { return true; }

	else { return false; }
}

//Player Turn (duh)
void playerTurn(Unit* player, vector<Unit*> &targets)
{
	int decision;

	cout << "Choose your Move of Action" << endl;
	cout << "=============================" << endl;
	cout << endl;

	cout << "1. Basic Attack" << endl;
	cout << "2. " << player->getSkillName() << "MP Cost: " << player->getManaCost() << endl;

	for (;;)
	{
		cin >> decision;
		//Decision 1 - Basic Attack
		if (decision == 1)
		{
			//Engaging Attack
			Unit* target = locateTarget(player, targets);
			//Damage Calculator
			int damage = player->damageComputation(player, target);

			//possible chance of Critical
			if (player->hitAttack(player, target)){
				if (criticalHit(player, target)){

					damage *= 1.20;
					cout << " Critical Hit! " << endl;
				}

				cout << player->getName() << " attacked " << target->getName() << " for " << damage << " damage! " << endl;
				target->takeDamage(damage);
			}
		
			else { cout << "You Missed" << endl; }
			//breaking the Forever Loop
			break;
		}
		//Decision 2 - Skill Attack
		else if (decision == 2)
		{
			if (player->getManaCost() > player->getMp()) { cout << "Not enough Mana" << endl; }

			else{ 
				player->skill(player, targets);
				break;
			}
		}

		else { cout << "Input Denied" << endl; }
	}
}

// Enemy Turn
void enemyTurn(Unit * enemy, vector<Unit*>& targets)
{
	int choice = 1;
	if (enemy->getMp() >= enemy->getManaCost()) { choice = rand() % 2 + 1; }

	//Decision 1
	if (choice == 1){
		// same old same old
		Unit* target = locateTarget(enemy, targets);
		//^
		int damage = enemy->damageComputation(enemy, target); 

		if (enemy->hitAttack(enemy, target)){

			if (criticalHit(enemy, target)){
				damage *= 1.20;
				cout << " Critical Damage! " << endl;
			}

			cout << enemy->getName() << " attacked " << target->getName() << " for " << damage << " damage! " << endl;
			target->takeDamage(damage);
		}

		else { cout << " Missed! " << endl; }
	}

	//Decision 2
	else if (choice == 2) { enemy->skill(enemy, targets); }
}

//Team Sides
void teamSides(vector<Unit*> &side)
{
	//Displays the Team Names for Aesthethic Purpose
	cout << "Team: " << side[0]->getTeamSide() << endl;
	cout << "==============================" << endl;

	for (int j = 0; j < 3; j++){
		//If Unit's still Alive
		if (side[j]->fate()) { cout << side[j]->getName() << " HP: " << side[j]->getHp() << "/" << side[j]->getMaxHp() << endl; }
		//or Dead
		else { cout << "Dead" << endl; }
	}
}

//The Battle 
void playTurn(vector<Unit*> player, vector<Unit*> enemy)
{
	//Turn Orders
	vector<Unit*> turnOrder = Order(player, enemy);

	//Declaration for the Team Winners
	int playerTeam = 0;
	int enemyTeam = 0;

	//Wuht kind of black magicz iz thiz?
	for(;;){

		teamSides(player);
		cout << endl;
		cout << " vs " << endl << endl;
		teamSides(enemy);
		cout << endl;

		Unit* chance = turnOrder[0];
		turnOrderPrint(turnOrder);

		cout << "Current Turn: " << chance->getName() << endl;
		system("pause") << system("cls");

		cout << chance->getName() << endl;
		turnOrder[0]->printStats();
		cout << endl;
	
		//Turn Order for Player Side
		if (turnOrder[0]->getTeamSide() == player[0]->getTeamSide()) { playerTurn(chance, turnOrder); }
		//Turn Order for the Enemy Side
		else { enemyTurn(chance, turnOrder); }
		
		//If the Health of a Unit is beyond or equal to zero, he's dead and thus will be removed from the turn order
		for (int j = 0; j < turnOrder.size(); j++){
			if (!turnOrder[j]->fate()){
				//Player
				if (turnOrder[j]->getTeamSide() == player[0]->getTeamSide()) { playerTeam++; }
				//Enemy
				else { enemyTeam++; }
				//Erasing the Unit/s from the turn order
				turnOrder.erase(turnOrder.begin() + j);
			}
		}

		//breaking the forever loop
		if (playerTeam == 3 || enemyTeam == 3) { break; }

		//rotate (duh)
		rotate(turnOrder.begin(), turnOrder.begin() + 1, turnOrder.end());
		
		system("pause") << system("cls");
	}
	system("pause") << system("cls");

	teamSides(player);
	cout << endl;
	cout << "VS" << endl << endl;
	teamSides(enemy);
	cout << endl;

	system("pause") << system("cls");
	
	//Epilogue of the Game
	//Player Wins

	if (playerTeam == 3) { cout << enemy[0]->getTeamSide() << " won!" << endl; }
	//Enemy Wins
	else if (enemyTeam == 3) { cout << player[0]->getTeamSide() << " won!" << endl; }

}

//Teh Compiler for all this sh|ts to work
int main()
{
	srand(time(NULL));

	int input = 0;

	string playerTeam = " Team Grand Chase ";
	string enemyTeam = " Team Kaze Aaze ";

	vector<Unit*> sideA;
	Unit* a = new Unit("Elesis", playerTeam, 1);
	sideA.push_back(a);
	Unit* b = new Unit("Lire", playerTeam, 2);
	sideA.push_back(b);
	Unit* c = new Unit("Arme", playerTeam, 3);
	sideA.push_back(c);

	vector<Unit*> sideB;
	Unit* x = new Unit("Ronan", enemyTeam, 1);
	sideB.push_back(x);
	Unit* y = new Unit("Lass", enemyTeam, 2);
	sideB.push_back(y);
	Unit* z = new Unit("Kaze Aaze", enemyTeam, 3);
	sideB.push_back(z);

	menuCheck(input, sideA, sideB);
	for (int g = 0; g < sideA.size(); g++)
	{
		delete sideA[g];
		delete sideB[g];
	}

	sideA.erase(sideA.begin(), sideA.end());
	sideB.erase(sideB.begin(), sideB.end());

	system("pause");
	return (0);

}